package Authorization;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

public class AuthorizationPage extends AbstractPage {

    @FindBy(xpath = "//label[contains(.,'Username')]//input")
    private WebElement userNameInput;

    @FindBy(xpath = "//label[contains(.,'Password')]//input")
    private WebElement passwordInput;

    @FindBy(xpath = "//button[contains(.,'Login')]")
    private WebElement loginButton;

    public AuthorizationPage(WebDriver driver) {

        super(driver);
    }

    public AuthorizationPage setUserName(String userName) {
        this.userNameInput.click();
        this.userNameInput.sendKeys(userName);
        return this;
    }

    public AuthorizationPage setPassword(String password) {
        this.passwordInput.click();
        this.passwordInput.sendKeys(password);
        return this;
    }

    public void loginIn() {

        this.loginButton.click();
    }

    public void loginIn(String userName, String password){

        Actions loginIn = new Actions(getDriver());
        loginIn
                .click(this.userNameInput)
                .sendKeys(userName)
                .click(this.passwordInput)
                .sendKeys(password)
                .click(this.loginButton)
                .build()
                .perform();
    }
}




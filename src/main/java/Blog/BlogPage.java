package Blog;

import Authorization.AbstractPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class BlogPage extends AbstractPage {
    private WebDriver driver;

    @FindBy(xpath = "//a[@class='logo svelte-1rc85o5']")
    private WebElement homeButton;

    @FindBy(xpath = "//a[contains(.,'Next Page')]")
    private WebElement buttonNextPage;

    @FindBy(xpath = "//a[contains(text(), 'Previous Page')]")
    private WebElement buttonPreviousPage;

    @FindBy(xpath = "//h1[@class='svelte-127jg4t'][contains(.,'Blog')]")
    private WebElement imageFirstPost;

    @FindBy(xpath = "//h1[@class='svelte-d01pfs disabled'][contains(.,'Blog')]")
    private WebElement titleFirstPost;

    @FindBy(xpath = "//h1[@class='svelte-127jg4t'][contains(.,'Blog')]")
    private WebElement descriptionFirstPost;

    public BlogPage(WebDriver driver) {
        super(driver);
    }

    public void goToHomePage() {
        homeButton.click();
        new WebDriverWait(getDriver(), Duration.ofSeconds(5)).until(ExpectedConditions.urlContains("https://test-stand.gb.ru/"));
    }

    public void goToNextPage() {
        buttonNextPage.click();
        new WebDriverWait(getDriver(), Duration.ofSeconds(5)).until(ExpectedConditions.urlContains("https://test-stand.gb.ru/"));
    }

    public void goToPreviousPage() {
        buttonPreviousPage.click();
        new WebDriverWait(getDriver(), Duration.ofSeconds(5)).until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//span[contains(.,'Home')]")));
    }

    public void imageFirstPost(){

        new WebDriverWait(getDriver(), Duration.ofSeconds(5)).until(ExpectedConditions.urlContains("https://test-stand.gb.ru/"));
    }

    public void titleFirstPost(){

        new WebDriverWait(getDriver(), Duration.ofSeconds(5)).until(ExpectedConditions.urlContains("https://test-stand.gb.ru/"));
    }

    public void descriptionFirstPost() {
        new WebDriverWait(getDriver(), Duration.ofSeconds(5)).until(ExpectedConditions.urlContains("https://test-stand.gb.ru/"));
    }
}


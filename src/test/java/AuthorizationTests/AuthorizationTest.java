package AuthorizationTests;

import Authorization.AuthorizationPage;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;

//import static org.junit.jupiter.api.Assertions.assertEquals;

public class AuthorizationTest extends AbstractTest {

    @Test
    @DisplayName("Авторизация пользователя. Валидный пароль и валидный логин")
    @Description("Проверка авторизации пользователя с помощью логина и пароля")
    @Severity(SeverityLevel.MINOR)
    void loginIn(){
        new AuthorizationPage(getWebDriver())
                .setUserName("Mary Mary")
                .setPassword("d9aed81b69")
                .loginIn();
        Assertions.assertTrue(getWebDriver().findElement(By.xpath("//a[contains(.,'Hello, Mary Mary')]")).isDisplayed());
    }

    @Test
    @DisplayName("Страница пользователя открылась после успешной авторизации")
    @Description("После авторизации проверка, что открылась страница пользователя")
    @Severity(SeverityLevel.MINOR)
    void loginInSecond() {
        new AuthorizationPage(getWebDriver())
                .loginIn("Mary Mary","d9aed81b69");
        Assertions.assertTrue(getWebDriver().findElement(By.xpath("//a[contains(.,'Hello, Mary Mary')]")).isDisplayed());
    }

    @Test
    @DisplayName("Авторизация пользователя. Проверка граничных значений")
    @Description("Проверка авторизации пользователя с помощью логина длинной 3 символа и валидного пароля")
    @Severity(SeverityLevel.MINOR)
     void loginThreeChar(){
        new AuthorizationPage(getWebDriver())
                .setUserName("tri")
                .setPassword("d2cfe69af2")
                .loginIn();
        Assertions.assertTrue(getWebDriver().findElement(By.xpath("//a[contains(.,'Hello, tri')]")).isDisplayed());
    }

    @Test
    @DisplayName("Авторизация пользователя. Проверка граничных значений")
    @Description("Проверка авторизации пользователя с помощью логина длинной 4 символа и валидного пароля")
    @Severity(SeverityLevel.MINOR)
    void loginFourChar() {
        new AuthorizationPage(getWebDriver())
                .setUserName("vvvv")
                .setPassword("386c57017f")
                .loginIn();
        Assertions.assertTrue(getWebDriver().findElement(By.xpath("//a[contains(.,'Hello, vvvv')]")).isDisplayed());
    }

    @Test
    @DisplayName("Авторизация пользователя. Проверка граничных значений")
    @Description("Проверка авторизации пользователя с помощью логина длинной 19 символов и валидного пароля")
    @Severity(SeverityLevel.MINOR)
    void loginNineteenthChar() {
        new AuthorizationPage(getWebDriver())
                .setUserName("devatnadzatsimvolov")
                .setPassword("379fea1c4e")
                .loginIn();
        Assertions.assertTrue(getWebDriver().findElement(By.xpath("//a[contains(.,'Hello, devatnadzatsimvolov')]")).isDisplayed());
    }

    @Test
    @DisplayName("Авторизация пользователя. Проверка граничных значений")
    @Description("Проверка авторизации пользователя с помощью логина длинной 20 символов и валидного пароля")
    @Severity(SeverityLevel.MINOR)
    void loginTwentyChar() {
        new AuthorizationPage(getWebDriver())
                .setUserName("dvadzatsimvolovcheck")
                .setPassword("aa95d0e7c4")
                .loginIn();
        Assertions.assertTrue(getWebDriver().findElement(By.xpath("//a[contains(.,'Hello, dvadzatsimvolovcheck')]")).isDisplayed());
    }

    @Test
    @DisplayName("Авторизация пользователя. Невалидные данные(незарегестрированный пользователь")
    @Description("Проверка авторизации пользователя с невалидными данными")
    @Severity(SeverityLevel.MINOR)
    void loginFail() throws InterruptedException {
        new AuthorizationPage(getWebDriver())
                .loginIn("NegativeTest","NegativeTest");
        Thread.sleep(2000);
        Assertions.assertTrue(getWebDriver().findElement(By.xpath("//p[contains(.,'Invalid credentials.')]")).isDisplayed());
    }

    @Test
    @DisplayName("Негативный тест. Проверка пользователя с коротким логином")
        @Description("Проверка авторизации пользователя с невалидным логином(2 символа)")
        @Severity(SeverityLevel.MINOR)
        void loginFailShortLog() {
        new AuthorizationPage(getWebDriver())
                .loginIn("tw","255a5cac76");
        Assertions.assertTrue(getWebDriver().findElement(By.xpath("//p[contains(.,'Invalid credentials.')]")).isDisplayed());

    }

    @Test
    @DisplayName("Негативный тест. Авторизация пользователя с с длинным логином")
    @Description("Проверка авторизации пользователя с невалидным логином(21 символ)")
    @Severity(SeverityLevel.MINOR)
    void loginFailLongLog() {
        new AuthorizationPage(getWebDriver())
                .loginIn("dlinaloginabolshechem","ef248d366e");
        Assertions.assertTrue(getWebDriver().findElement(By.xpath("//p[contains(.,'Invalid credentials.')]")).isDisplayed());
    }

    @Test
    @DisplayName("Негативный тест. Авторизация с невалидным паролем")
    @Description("Проверка авторизации пользователя с валидным логином и невалидным паролем")
    @Severity(SeverityLevel.MINOR)
    void loginUnRegisteredLongLogin() {
        new AuthorizationPage(getWebDriver())
                .loginIn("Mary Mary"," ");
        Assertions.assertTrue(getWebDriver().findElement(By.xpath("//p[contains(.,'Invalid credentials.')]")).isDisplayed());
    }
}



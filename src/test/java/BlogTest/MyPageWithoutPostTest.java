package BlogTest;

import Authorization.AuthorizationPage;
import io.github.bonigarcia.wdm.WebDriverManager;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.junit.jupiter.api.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import java.time.Duration;

public class MyPageWithoutPostTest {

        static EventFiringWebDriver eventDriver;

        @BeforeAll
        static void init(){
            WebDriverManager.firefoxdriver().setup();
            FirefoxOptions options = new FirefoxOptions();

            eventDriver = new EventFiringWebDriver(new FirefoxDriver(options));
            eventDriver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5));

        }

        @BeforeEach
        void goTo(){
            Assertions.assertDoesNotThrow(()-> eventDriver.navigate().to("https://test-stand.gb.ru/login"),
                    "Страница не доступна");
        }

        @AfterAll
        public static void exit(){
             if(eventDriver !=null) eventDriver.quit();
        }

        public WebDriver getWebDriver(){
            return eventDriver;
        }

    @Test
    @DisplayName("Проверка страницы пользователя, у которого отсутствуют посты")
    @Description("Отсутствие публикаций у пользователя, у которого нет постов")
    @Severity(SeverityLevel.MINOR)
    void checkNoHavePost(){

        new AuthorizationPage(getWebDriver())
                .loginIn("noHavePost","c49146bd5e");
        Assertions.assertTrue(getWebDriver().findElement(By.xpath("//p[contains(.,'No items for your filter')]")).isDisplayed());
    }
}

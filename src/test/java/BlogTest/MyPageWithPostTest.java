package BlogTest;

import Blog.BlogPage;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MyPageWithPostTest extends AbstractBlogTest {

    @Test
    @DisplayName("Проверка картинки у первого поста")
    @Description("При открытии ленты со своими постами, проверяем что у первого поста есть картинка")
    @Severity(SeverityLevel.MINOR)
    void imagePost() throws InterruptedException {

        new BlogPage(getWebDriver()).imageFirstPost();
        Thread.sleep(2000);
        Assertions.assertTrue(getWebDriver().findElement(By.xpath("//img[@src='http://test-stand.gb.ru/files/public/image/3edd50b99a773aa95762d4615cc895a8.jpg']")).isDisplayed());
    }

    @Test
    @DisplayName("Проверка заголовка у первого поста")
    @Description("При открытии ленты со своими постами, проверяем что у первого поста есть заголовок")
    @Severity(SeverityLevel.MINOR)
    void titlePost(){

        new BlogPage(getWebDriver()).titleFirstPost();
        Assertions.assertTrue(By.xpath("//h2[contains(.,'ТестовыйКот 6')]").findElement(getWebDriver()).isDisplayed());
    }

    @Test
    @DisplayName("Проверка описания у первого поста")
    @Description("При открытии ленты со своими постами, проверяем что у первого поста есть описание")
    @Severity(SeverityLevel.MINOR)
    void descriptionPost() throws InterruptedException {

        new BlogPage(getWebDriver()).descriptionFirstPost();
        Thread.sleep(3000);
        Assertions.assertTrue(By.xpath("//div[@class='description svelte-127jg4t'][contains(.,'Здесь текст')]").findElement(getWebDriver()).isDisplayed());
    }

    @Test
    @DisplayName("Кнопка Главная")
    @Description("Проверка кнопки Главная")
    @Severity(SeverityLevel.MINOR)
    void clickToHomePage () {

        new BlogPage(getWebDriver()).goToHomePage();
        Assertions.assertTrue("Symfony Blog".equals(getWebDriver().getTitle()));
    }

    @Test
    @DisplayName("Кнопка Следующая страница")
    @Description("Следующая страница с постами открывается после нажатия кнопки Следующая страница")
    @Severity(SeverityLevel.MINOR)
    void clickToNextPage() throws InterruptedException {

        new BlogPage(getWebDriver()).goToNextPage();
        Thread.sleep(2000);
        assertEquals("https://test-stand.gb.ru/?page=2", getWebDriver().getCurrentUrl());
    }

    @Test
    @DisplayName("Кнопка Предыдущая страница")
    @Description("Предыдущая страница с постами открывается после нажатия кнопки Предыдущая страница. Переход на страницу 1")
    @Severity(SeverityLevel.MINOR)
    void clickToPreviousPage() throws InterruptedException {
        new BlogPage(eventDriver)
                .goToNextPage();
        Thread.sleep(3000);
        new BlogPage(eventDriver)
                .goToPreviousPage();
        Thread.sleep(3000);
        assertEquals("https://test-stand.gb.ru/?page=1", getWebDriver().getCurrentUrl());
    }
}

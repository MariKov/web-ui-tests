package BlogTest;

import Authorization.AuthorizationPage;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import java.time.Duration;

public abstract class AbstractBlogTest {

    static EventFiringWebDriver eventDriver;

    @BeforeAll
    static void init() throws InterruptedException {
        WebDriverManager.firefoxdriver().setup();
        FirefoxOptions options = new FirefoxOptions();
        options.addArguments("--incognito");
        options.addArguments("start-maximized");

        eventDriver = new EventFiringWebDriver(new FirefoxDriver(options));
        eventDriver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5));

        loginIn();

    }

    @DisplayName("Авторизация пользователя")
    static void loginIn() throws InterruptedException {
        eventDriver.navigate().to("https://test-stand.gb.ru/login");

        new AuthorizationPage(getWebDriver())
                .loginIn("Mary Mary","d9aed81b69");
        Thread.sleep(2000);

    }

    @AfterAll
    public static void exit(){
         if(eventDriver !=null) eventDriver.quit();
    }

    public static WebDriver getWebDriver(){
        return eventDriver;
    }

}
